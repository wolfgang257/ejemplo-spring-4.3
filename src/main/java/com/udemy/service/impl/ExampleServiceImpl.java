package com.udemy.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;


import com.udemy.model.Person;
import com.udemy.service.ExampleService;

@Service("exampleService")
public class ExampleServiceImpl implements ExampleService{
	
	private static final Log LOGGER = LogFactory.getLog(ExampleServiceImpl.class);

	@Override
	public List<Person> getListPeople() {
		// TODO Auto-generated method stub
		List<Person> people = new ArrayList<>();
		people.add(new Person("A", 1));
		people.add(new Person("B", 2));
		people.add(new Person("C", 3));
		people.add(new Person("D", 4));
		people.add(new Person("E", 5));
		
		LOGGER.info("From service");
		
		return people;
	}

}
