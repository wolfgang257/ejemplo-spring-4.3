package com.udemy.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.udemy.repository.CourseJpaRepository;
import com.udemy.service.EjercicioService;

@Service("ejercicioService")
public class EjercicioServiceImpl implements EjercicioService{

	private static final Log LOG = LogFactory.getLog(EjercicioServiceImpl.class);
	
	@Autowired
	@Qualifier("courseJpaRepository")
	private CourseJpaRepository courseJpaRepository;
	
	@Override
	public void llamarMetodo() {
		LOG.warn("Se ha llamado el método de "+this.getClass().getSimpleName());
		//courseJpaRepository.
		
	}

}
