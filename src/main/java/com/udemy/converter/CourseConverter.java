package com.udemy.converter;

import org.springframework.stereotype.Component;

import com.udemy.entity.Course;
import com.udemy.model.CourseModel;

@Component("courseConverter")
public class CourseConverter {

	/**
	 * Entity --> Model
	 * @param course
	 * @return
	 */
	public CourseModel entity2model(Course course){
		CourseModel model = new CourseModel(course.getId());
		model.setName(course.getName());
		model.setDescription(course.getDescription());
		model.setHours(course.getHours());
		model.setPrice(course.getPrice());
		
		return model;
	}
	
	/**
	 * Model --> Entity
	 * @param course
	 * @return
	 */
	public Course model2entity(CourseModel course){
		Course entity = new Course();
		entity.setId(course.getId());
		entity.setName(course.getName());
		entity.setDescription(course.getDescription());
		entity.setHours(course.getHours());
		entity.setPrice(course.getPrice());
		
		return entity;
	}
}
