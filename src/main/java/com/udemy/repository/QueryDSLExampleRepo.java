package com.udemy.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.udemy.entity.QCourse;
import com.udemy.entity.Course;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;;

@Repository("queryDSLExampleRepo")
public class QueryDSLExampleRepo {

	QCourse qCourse = QCourse.course;
	
	@PersistenceContext
	private EntityManager em;
	
	public Course find(boolean exist){
		
		JPAQuery<Course> query = new JPAQuery<Course>(em);
		
		BooleanBuilder predicateBuilder = new BooleanBuilder(qCourse.description.endsWith("OP"));
		if(exist){
			Predicate predicate2 = qCourse.id.eq(23);
			predicateBuilder.and(predicate2);
		}else{
			predicateBuilder.or(qCourse.name.endsWith("OP"));
		}
		
		return query.select(qCourse).from(qCourse).where(predicateBuilder).fetchOne();
		
		//List<Course> courses = query.select(qCourse).from(qCourse).where(qCourse.hours.between(13, 50)).fetch();
	}
}
