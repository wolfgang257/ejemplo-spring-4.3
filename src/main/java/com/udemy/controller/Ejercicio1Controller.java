package com.udemy.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.udemy.service.EjercicioService;

@Controller
@RequestMapping("/ejercicio")
public class Ejercicio1Controller {
	
	private static final Log LOG = LogFactory.getLog(Ejercicio1Controller.class);
	private static final String EJERCICIO_VIEW="ejercicio";
	
	@Autowired
	@Qualifier("ejercicioService")
	private EjercicioService ejercicioService;

	/**
	 * Para redireccionar a dos
	 * @return
	 */
	@GetMapping("/uno")
	public RedirectView redirect(){		
		return new RedirectView("/ejercicio/dos"); 
	}
	
	/**
	 * Para mostrar una vista
	 * @return
	 */
	@GetMapping("/dos")
	public ModelAndView exampleMAV(){
		long inicio = System.currentTimeMillis();
		
		ejercicioService.llamarMetodo();
		
		ModelAndView mav = new ModelAndView(EJERCICIO_VIEW);
		mav.addObject("mensaje", "Mensaje desde CO");
		
		LOG.info("Terminó petición en "+(System.currentTimeMillis() - inicio)+" ms!"); 
		return mav;
	}
}
