package com.udemy.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.udemy.converter.CourseConverter;
import com.udemy.entity.Course;
import com.udemy.model.CourseModel;
import com.udemy.service.CourseService;
import com.udemy.service.EjercicioService;

@Controller
@RequestMapping("/courses")
public class CourseController {
	
	private static final Log LOG = LogFactory.getLog(CourseController.class);
	
	private static final String COURSES_VIEW="courses";
	
	@Autowired
	@Qualifier("courseService")
	private CourseService courseService;
	
	@Autowired
	@Qualifier("courseConverter")
	private CourseConverter courseConverter;

	/**
	 * 
	 * @return
	 */
	@GetMapping("/listcourses")
	public ModelAndView listAllCourses(){	
		LOG.info("Call: listAllCourses");
		
		List<Course> courses = courseService.listAllCourses();
		LOG.info("Call: listAllCourses con list "+courses.size());
		
		ModelAndView mav = new ModelAndView(COURSES_VIEW); 
		mav.addObject("course", new CourseModel() );
		mav.addObject("courses", courses );
		return mav;
	}
	
	/**
	 * 
	 * @param course
	 * @return
	 */
	@PostMapping("/addcourse")
	public String addCourse(@ModelAttribute("course") CourseModel course){	
		LOG.info("Call: addCourse > "+course	);
		courseService.addCourse(courseConverter.model2entity(course));
		return "redirect:/courses/listcourses";
	}
	
	/**
	 * 
	 * @param course
	 * @return
	 */
	@PostMapping("/savecourse")
	public String saveCourse(@ModelAttribute("course") CourseModel course){	
		LOG.info("Call: saveCourse > "+course	);
		courseService.addCourse(courseConverter.model2entity(course));
		return "redirect:/courses/listcourses";
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	//@DeleteMapping("/deletecourse/{id}")
	@RequestMapping(value="/deletecourse/{id}", method= {RequestMethod.GET, RequestMethod.DELETE} )
	public String deletecourse(@PathVariable("id") Integer id){	
		LOG.info("Call: deletecourse > "+id	);
		courseService.removeCourse(id);
		return "redirect:/courses/listcourses";
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/getcourse/{id}")
	public ModelAndView getcourse(@PathVariable("id") Integer id){	
		LOG.info("Call: getcourse");
		
		List<Course> courses = courseService.listAllCourses();
		LOG.info("Call: getcourse con list "+courses.size());
		
		ModelAndView mav = new ModelAndView(COURSES_VIEW); 
		mav.addObject("course", courseConverter.entity2model(courseService.getCourse(id)) ); 
		mav.addObject("courses", courses );
		return mav;
	}
}
